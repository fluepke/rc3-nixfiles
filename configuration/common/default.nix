{ config, pkgs, lib, ... }:

{
  imports = [
    ./users.nix
    ./node-exporter.nix
    ./nginx.nix
    ./ssh.nix
    ./acme.nix
    ../../modules
  ];

  boot.kernelPackages = pkgs.linuxPackages_latest;

  security.sudo.wheelNeedsPassword = false;
  nix.trustedUsers = [ "@wheel" ]; # needed to deploy locally built packages

  environment.systemPackages = with pkgs; [
    wget vim htop nload fd ripgrep exa bat dnsutils
    tmux curl jq git socat 
  ];

  time.timeZone = "Etc/UTC";

  programs.bash = {
    shellAliases = {
      ".." = "cd ..";
      use = "nix-shell -p ";
      cat = "bat --style=header ";
      grep = "rg";
      ls = "exa";
      ll = "exa -l";
      la = "exa -la";
      tree = "exa -T";
    };
  };

  programs.mtr.enable = true;

  nixpkgs.config = {
    packageOverrides = import ../../pkgs { inherit pkgs; };
  };
}

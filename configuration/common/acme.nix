{ config, ... }:

{
  security.acme.email = "security@${config.networking.hostName}.${config.networking.domain}";
  security.acme.acceptTerms = true;
}

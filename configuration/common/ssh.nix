{ config, pkgs, lib, ... }:

let
  sshPort = 42023;
in
{
  networking.firewall.allowedTCPPorts = [ sshPort ];

  # enable tarpit SSH server on port 22
  services.tarssh.enable = true;
  services.tarssh.listenPort = 22;

  rc3.deploy.ssh.port =  sshPort;

  services.openssh = {
    enable = true;
    permitRootLogin = "yes";
    passwordAuthentication = false;
    challengeResponseAuthentication = false;
    extraConfig = ''
      AuthenticationMethods publickey
    '';
    ports = [ sshPort ];
    banner = let
      wheelUsers = with lib; filterAttrs (
        name: u: elem "wheel" u.extraGroups
      ) config.users.users;
      wheelUserNames = with lib; concatStringsSep "\n  - " (attrNames wheelUsers);
    in ''
      Welcome to ${config.networking.hostName}.${config.networking.domain}!
      This system is managed by Nix!

      Maintainers:
        - ${wheelUserNames}
    '';
  };

}

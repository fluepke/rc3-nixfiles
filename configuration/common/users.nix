{ ... }:

{
  users.users = {
    thies = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCyx9QTEROCHQh0lDH8nFzPNqZrrzKxsn+vuaDmpoGi2Jt1Ckl0+U4noDTN5IEslP3c5rhpolpoINQJFuWsgInLz8lziVu6bRJFGD/9qdZpzIVdzxX36Bh/IU7+siz7IbWhVDySBPiVj3f/gTlN5Z+jdyj46VY91+GhL5RpgMimoRVowYOlFamIUtA5oR5tu6ihD6ybaVRluzOAUhfFmmlFKDZr6RkhLoMqh5k+ZZVTdjDvoSMdPdZe6/gnhcJgO+bzVSo2xN1ak3J2M3VepLHYWUeiThgt94KvCS+OmG2K8IvGwPfc7DzOeqT1RtYl9nwWWz/NUwYpnJGadePIvBr/ tmueller@macmuell" ];
    };
    jmarienfeldt = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDCWa0Kzx8YYb5i1CHGouwpQzjK+nl9BIcH75b3Ngjgz6WVNN8I7C48WOve0GHcX7spiFL92CmEzRIXj+s/wsARWaaEUtfoUg44kTumOdtaQsISeBfpHLLgd4pvD5cerKHXSL1ZMkrjzT2xQ/X7BjOt2Af88U2lVj2a+euJz6/VcCeoQ3RgHCWVsjM/hjwRCWuMtqfHVb7klqYU76jwqERquuxGp7ChVLJecHirv1j95qpfu3W2EtUj0zbxvM+W46756unLeLRQHx878Pwqlkriv1YGY9rJWCCdOJ+4WOrUyLutvI52Tzq7A1JKVmtEEwZ3I7+tIr6NScWs+/rdPS3aXuDnhmQ1Kc/elHb4KHmRANREtPBUuf35G4tyLUJXUlKChSOMmyC7shkV3bGgN3GYsDomxAKVEuFnSeFLAfJB1CCWBMqxtXpRMqEd5K1EVFQ9oGRflAuhHHVpcvgBy6tvGJK7z7PSudy7TmwghG01Y/icdvfyhFI2HEC7d5fVgiiVl98xjw2pK09SmRfD443KyAiJaWqGLjv78QrdoAu7LxrYCbcKQ7tPYv+eTFBGC/GF4RM5akyqfporsQYYQ/r3HSjF03oWS2gm1rPhnSV7Rg/YhnrrrONUgpgJvo3J3rdIY8jR9idgDHcpYQUCMDzscHKjnYPC6XnxdPmVvJrNQQ== jmarienfeldt@maci.local" ];
    };
    fluepke = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIFPW/LUTmJn5Ip8x5HjrjENjCh+u9aA60uGzLpNsBag cardno:000611180084" ];
    };
    pbb = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDRwBqKRQVVNKNN9XXlcumgGjdvo8WVujZGvWtnyvsBRPJoRQxgO6ONKM9GAnXEDok+OZE2/GNvLW08K0k7IL9xk4OVtttjQ2Qj0I4na6z4MmTUVtMspaxQup6wnXGxKIdKrMNKM7O8zrpHFusUym2dnYNSf06yIk55MZkOdCePkpdDEBCwDcqQFrE9Jg0jofdIvOdcVPzisJEwny/QGN30auWN2imr+Ij0Wqhp79Q1x4VT6f8uje2e8V5k0ZZYApDB+dankAtTc38FBRu5utVmKrSrEPEb8oPQN0YNQ6o/cQrrQr2r4AImS+FQRohIkliLt7FNkvigqie1ebU3w9XC2xGuUxlyLTKGxXoiQIk3j/XY07X4fG8/pcIndaT+C5kkejmoZ2yKZqxCHENDP+77lkcVGoE5VrX//C/YPsyGG06JfzZ29oBvIGPuPoehdyiFh0hMP6D2XLmt6K688z3tRgSV/CG8z4QKEoVGOTi0my4Wahi/kb4PQsmSosRC2GzHxueZjK7e3h35OW+keXbZlIULTRPWBk1JSoha13QAS1sLNZlks35DktOyV2YyOuXHZ9y5WYQL2+E2jIVE6FzSC84Aoaj+Kq64tE/Ey9AT4cu+zb0DNEiIKtiaKzZZ8a3pyVbXLripJTAYPFr8L/SS6H7b5G4Oi3wxyap5ACQ5JQ== cardno:000605977604" ];
    };
    waschtl = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMNjEfNBpZPB8ATdY2bQtBYjXss4GEzIF2FV7Cx9RMYC waschtl" ];
    };
    marass = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCoFPhkGFDLn+E24nvJqTm/B3d12JY4bztujWR+NsE9tfglHcdGZX6T50pOaRMekwEld6b+D29FnfuFSzsfTGz+BAK29XWAJrQuUjvXqN7xxhoZ/o/f7/HLjWleSXv9hSC7P54PnwnVPxkf7lwnX1UqbTYc3xdwCN3FBZ1tR9RMbWrk/T3t8B/GkwGL3J9HQknmptAeqNZuvUa3TtERdNIWS9RmIUCbUAnbMqlYQamHVNPQkzOXhbDN68O+RrbGs+Fv8NpdAQkg1VPReIVWCEkBKvErA3aABpplgnit/JX9Y5WJ3/+ccwG/S2BmNOEwNg2qRhgq5FeIzTCTLy8wo3KJtIsDuEt3XlnDRxUCPbb5PXvs7djEcuq30I3xk1cOlAXIbHVjV+wu9/ErwoaCtU6XSydP0luVIXROgOdPDV5fChUZJdVzkTuHEuUCP9w1HhV1pzUFBOIUgvzn8Bd+FPNX+9k2vP+oGN80r7y0DoTh/4Q5Ide8SCl91/Xgla6O98E= marass" ];
    };
    nd = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIK1glUUBoilF3in24sAhxpjKlPoWTSQrUhG6mPpqTOfT nd"];
    }; 
  };
}

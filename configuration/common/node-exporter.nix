{ config, ... }:

let
  listenAddress = "[::1]";
  nodeExporterPort = toString config.services.prometheus.exporters.node.port;
  nginxExporterPort = toString config.services.prometheus.exporters.nginx.port;
  fqdn = "${config.networking.hostName}.${config.networking.domain}";

in
{
    services.prometheus.exporters.node = {
    enable = true;
    inherit listenAddress;
  };

  services.prometheus.exporters.nginx = {
    enable = true;
    inherit listenAddress;
  };

  services.nginx.statusPage = true;
  services.nginx.enable = true;
  services.nginx.virtualHosts.${fqdn} = {
    enableACME = true;
    forceSSL = true;
    locations."/node-exporter/metrics".proxyPass = "http://${listenAddress}:${nodeExporterPort}/metrics";
    locations."/nginx-exporter/metrics".proxyPass = "http://${listenAddress}:${nginxExporterPort}/metrics";
  };
}

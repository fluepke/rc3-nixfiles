{ pkgs, dns }:

with pkgs.lib;

let
  inherit (import ../../lib/hosts.nix { inherit pkgs; }) hosts;
  secondaries = mapAttrsToList (
    name: host: "${host.config.networking.hostName}.${host.config.networking.domain}."
  ) (
    filterAttrs (
      name: host: !host.config.rc3.dns.isPrimary && builtins.elem "dns" host.config.rc3.deploy.groups
    ) hosts
  );
in

with dns.combinators;

{
  SOA = let
    primaryHost = findFirst (
      host: host.config.rc3.dns.isPrimary
    ) builtins.abort (
      attrValues hosts
    );
    primaryFqdn = "${primaryHost.config.networking.hostName}.${primaryHost.config.networking.domain}.";

  in {
    nameServer = primaryFqdn;

    adminEmail = "dnsmaster@${primaryFqdn}"; # Email address with a real `@`!
    serial = 2020112400;
  };

  NS = secondaries;

  TXT = imap (
    i: line: "${fixedWidthNumber 3 i} ${line}"
  ) (
    splitString "\n" (
      fileContents ./ascii_art.txt
    )
  );

  subdomains = let
    getAddresses = host: type: concatLists (
      mapAttrsToList (
        name: iface: map (
          addr: addr.address
        ) (
          iface.${type}.addresses
        )
      ) (
        filterAttrs (
          name: iface: name != "lo"
        ) host.config.networking.interfaces
      )
    );

  in (mapAttrs (name: host: {
    AAAA  = getAddresses host "ipv6";
    A     = getAddresses host "ipv4";
    CAA   = letsEncrypt "admin@${name}.${host.config.networking.domain}";
    SSHFP = host.config.rc3.dns.sshfp;
    subdomains = genAttrs host.config.rc3.dns.aliases (alias: {
      CNAME = [ "${name}.${host.config.networking.domain}." ];
    });
  }) hosts) // {
    backend = {
      NS = secondaries;
    };
    dynamic = {
      NS = secondaries;
    };
    infra = {
      NS = secondaries;
    };
    web = {
      NS = secondaries;
    };
    dev = {
      NS = secondaries;
    };
    org = {
      NS = secondaries;
    };
    howto = {
      CNAME = [ "in.web.rc3.world." ];
      subdomains = {
        fb = {
          CNAME = [ "in.web.rc3.world." ];
        };
      };
    };
    logogenerator = {
      A = [ "185.26.156.63" ];
      AAAA = [ "2a00:d0c0:200:0:b9:1a:9c:3e" ];
    };
    style = {
      A = [ "168.119.228.61" ];
      AAAA = [ "2a01:4f8:c2c:f5b9::1" ];
    };
    int = {
      NS = secondaries;
    };
    styleguide = {
      CNAME = [ "in.web.rc3.world." ];
    };
    merch = {
      A = [ "92.51.182.178" ];
    };
    tiles = {
      CNAME = [ "in.web.rc3.world." ];
    };
  };
}

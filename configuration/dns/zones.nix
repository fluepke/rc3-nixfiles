{ sources, pkgs, lib, config, ... }:

with lib;

let
  dns = import sources.nix-dns { inherit pkgs; };

in {
  rc3.dns.zones = {
    "rc3.world" = {
      file = dns.writeZone "rc3.world" (import ./rc3.world.zone.nix { inherit pkgs dns; });
    };
    "backend.rc3.world" = {
      file = ./zonefiles/backend.rc3.world.zone;
    };
    "infra.rc3.world" = {
      file = ./zonefiles/infra.rc3.world.zone;
    };
    "web.rc3.world" = {
      file = ./zonefiles/web.rc3.world.zone;
    };
    "dev.rc3.world" = {
      file = ./zonefiles/dev.rc3.world.zone;
    };
    "org.rc3.world" = {
      file = ./zonefiles/org.rc3.world.zone;
    };
    "int.rc3.world" = {
      file = ./zonefiles/int.rc3.world.zone;
    };
  };
}

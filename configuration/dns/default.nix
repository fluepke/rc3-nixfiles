{ pkgs, lib, ... }:

with lib;

let
  inherit (import ../../lib/hosts.nix { inherit pkgs; }) hosts;
  getIpAddresses = hostConfig: concatLists (
    mapAttrsToList (
      name: iface: map (
        addr: addr.address
      ) (
        iface.ipv4.addresses ++ iface.ipv6.addresses
      )
    ) (
      filterAttrs (
        name: iface: name != "lo"
      ) hostConfig.networking.interfaces
    )
  );
  getIPs = primary: concatLists (
    mapAttrsToList (
      name: host: getIpAddresses host.config
    ) (
      filterAttrs (
        name: host: (primary == host.config.rc3.dns.isPrimary) && (elem "dns" host.config.rc3.deploy.groups)
      ) hosts
    )
  );
  primaryIPs = getIPs true;
  secondaryIPs = getIPs false;
in
{
  rc3.deploy.groups = [ "dns" ];

  rc3.dns = {
    enable = true;
    primaryFqdn = "ns1.rc3.world";
    inherit primaryIPs secondaryIPs;
  };

  rc3.powerdns-auth-proxy.apiUsers = {
    visit = {
      keyFile = "/var/src/secrets/powerdns/api/visit";
      allowSuffixCreation = [ "visit.at.rc3.world." ];
    };
    api = {
      keyFile = "/var/src/secrets/powerdns/api/api";
      allowSuffixCreation = [ "api.at.rc3.world." ];
    };
    maps = {
      keyFile = "/var/src/secrets/powerdns/api/maps";
      allowSuffixCreation = [ "maps.at.rc3.world." ];
    };
    at = {
      keyFile = "/var/src/secrets/powerdns/api/at";
      allowSuffixCreation = [ "at.rc3.world." ];
    };
    pusher = {
      keyFile = "/var/src/secrets/powerdns/api/pusher";
      allowSuffixCreation = [ "pusher.at.rc3.world." ];
    };
    hub = {
      keyFile = "/var/src/secrets/powerdns/api/hub";
      allowSuffixCreation = [ "hub.rc3.world." ];
    };
  };
}

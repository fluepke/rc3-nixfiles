{ config, pkgs, lib, ... }:

{
  services.cachet.enable = true;
  services.cachet.hostName = "${config.networking.hostName}.${config.networking.domain}";
  services.cachet.appKey = "8hABua6e66MBmlQWxIMmLuHKyGCso09g";
}

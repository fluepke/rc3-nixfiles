{ config, lib, ... }:

{
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/disk/by-id/scsi-0QEMU_QEMU_HARDDISK_drive-scsi0-0-0-0";
  boot.initrd.availableKernelModules = lib.optional config.boot.initrd.network.enable "virtio-pci";

  networking.nameservers = [
    "2a02:2970:1002::18" "46.182.19.48" # digitalcourage
  ];
}

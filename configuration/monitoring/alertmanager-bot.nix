{ config, lib, ... }:

with lib;

let
  fqdn = config.rc3.monitoring.alertFqdn;
in
{
  services.alertmanager-bot = {
    enable = true;
    alertmanagerUrl = "http://${fqdn}${config.rc3.monitoring.alertLocation}";
    telegramAdmins = [
      "551779987" # fluepke
      "12524350"  # td00
    ];
    telegramTokenFile = "/var/src/secrets/alertmanager-bot/telegram.token";
    store = "bolt";
  };
  services.nginx.virtualHosts.${fqdn} = {
    locations."/alertmanager-bot/".proxyPass = let
      addr = config.services.alertmanager-bot.listenAddress;
    in "http://${addr}";
  };
}

{ config, lib, ... }:

with lib;

let
  fqdn = "${config.networking.hostName}.${config.networking.domain}";
in
{
  services.prometheus-smokeping-exporter = {
    enable = true;
    hosts = import ./smokeping-hosts.nix;
  };
  services.nginx.virtualHosts.${fqdn} = {
    locations."/smokeping-exporter/metrics".proxyPass = let
      addr = config.services.prometheus-smokeping-exporter.listenAddress;
    in "http://${addr}/metrics";
  };
}

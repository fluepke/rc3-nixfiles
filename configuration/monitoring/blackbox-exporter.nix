{ config, ... }:

let
  fqdn = "${config.networking.hostName}.${config.networking.domain}";
in
{
  services.prometheus.exporters.blackbox = {
    enable = true;
    listenAddress = "[::1]";
    configFile = ./blackbox-exporter.yml;
  };

  services.nginx.virtualHosts.${fqdn} = {
    locations."/blackbox-exporter/".proxyPass = let
      addr = config.services.prometheus.exporters.blackbox.listenAddress;
      port = toString config.services.prometheus.exporters.blackbox.port;
    in "http://${addr}:${port}/";
  };
}

{ config, lib, ... }:

with lib;

let
  fqdn = "${config.networking.hostName}.${config.networking.domain}";
in
{
  services.prometheus-junos-exporter = {
    enable = true;
    sshTargets = import ./junos-hosts.nix;
    sshKeyFile = "/var/src/secrets/junos_exporter/id_ed25519";
  };
  services.nginx.virtualHosts.${fqdn} = {
    locations."/junos-exporter/metrics".proxyPass = let
      addr = config.services.prometheus-junos-exporter.listenAddress;
    in "http://${addr}/metrics";
  };
  services.prometheus.scrapeConfigs = [{
    job_name = "junos";
    scheme = "https";
    metrics_path = "/junos-exporter/metrics";
    scrape_interval = "1m";
    scrape_timeout = "1m";
    static_configs = [{
        targets = import ./junos-hosts.nix;
    }];
    relabel_configs = [
      {
        source_labels = [ "__address__" ];
        target_label = "__param_target";
      }
      {
        source_labels = [ "__param_target" ];
        target_label = "instance";
      }
      {
        replacement = fqdn;
        target_label = "__address__";
      }
    ];
  }];
}

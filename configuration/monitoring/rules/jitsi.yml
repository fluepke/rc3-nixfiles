---

groups:
  - name: jitsi
    rules:
      - alert: JitsiJvbStressLevelTooHigh
        expr: jitsi_stress_level{app="jitsi", environment="prod"} > 1.05
        for: 10m
        labels:
          severity: warning
        annotations:
          summary: Jitsi JVB Stress level too high
          description: Check why Octo isn’t load balancing participants off this bridge. Check load on other JVBs in the same shard, check jicofo logs for octo errors
          dashboard: https://grafana.monitoring.rc3.world/d/iq3GEZbGz/rc3-jitsi-overview
   
      - alert: JitsiStressLevelsNotReflectingCpuUsageTooLow
        expr: sum by (instance) (jitsi_stress_level{app="jitsi", environment="prod"}) / (sum by (instance) (rate(node_cpu_seconds_total{mode!="idle",app="jitsi", environment="prod"}[2m])) / count by(instance) (node_cpu_seconds_total{mode="idle",app="jitsi", environment="prod"})) > 2
        for: 10m
        labels:
          severity: warning
        annotations:
          summary: Stress levels not accurately reflecting cpu usage - set too low
          description: The stress level is calculated by the load limit packet rate, that the JVB is processing. We’re comparing the reported stress level that Octo is using for load balancing with the cpu usage here. This alert is configured to fire if the limit is configured too low and can be set higher
   
      - alert: JitsiStressLevelsNotReflectingCpuUsageTooHigh
        expr: (sum by (instance) (jitsi_stress_level{app="jitsi", environment="prod"}) / (sum by (instance) (rate(node_cpu_seconds_total{mode!="idle",app="jitsi", environment="prod"}[2m])) / count by(instance) (node_cpu_seconds_total{mode="idle",app="jitsi", environment="prod"})) < 1.05) != 0
        for: 10m
        labels:
          severity: critical
        annotations:
          summary: Stress levels not accurately reflecting cpu usage - set too high
          description: The packet rate for this JVB is too optimistic and Octo will run load balance too late. Set the load limit for this JVB lower.
   
      - alert: JitsiJvbUnderperforming
        expr: ((sum by (shard) (jicofo_participants{environment="prod",shard=~".+"})) / (sum by (shard) (rate(node_cpu_seconds_total{mode!="idle", role="jvb",shard=~".+"}[1m])) / count by (shard) (node_cpu_seconds_total{mode="idle", shard=~".+",role="jvb"})) < 4) != 0
        for: 20m
        labels:
          severity: critical
        annotations:
          summary: JVBs are underperforming
          description: We can support less than 4 users per JVB core. We probably need more JVBs, or someone is doing weird things with the JVBs

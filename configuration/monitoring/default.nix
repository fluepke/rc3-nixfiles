{ pkgs, lib, ... }:

{
  imports = [
    ./prometheus.nix
    ./alertmanager-bot.nix
    ./smokeping-exporter.nix
    ./blackbox-exporter.nix
    ./junos-exporter.nix
  ];

  rc3.deploy.groups = [ "monitoring-cluster" ];
  rc3.monitoring.enable = true;
}

{ config, pkgs, lib, ... }:

with lib;

{
  services.prometheus.ruleFiles = [
    ./rules/blackbox.yml
    ./rules/nginx.yml
    ./rules/node.yml
    ./rules/prometheus.yml
    ./rules/jitsi.yml
    ./rules/powerdns.yml
    ./rules/postgres.yml
    ./rules/bbb.yml
    ./rules/workadventure.yml
  ];
}

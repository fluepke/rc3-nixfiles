# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/profiles/qemu-guest.nix")
    ];

  boot.initrd.availableKernelModules = [ "ata_piix" "virtio_pci" "virtio_scsi" "xhci_pci" "sd_mod" "sr_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/8cbccc18-3b86-49c9-a21f-46ed22fe4cd5";
      fsType = "xfs";
    };

  boot.initrd.luks.devices."crypto".device = "/dev/disk/by-uuid/f908fa37-e498-4972-a24f-6bcf34661746";

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/1937040c-d631-4641-bd35-6b1c0e97609c";
      fsType = "ext2";
    };

  swapDevices = [ ];

}

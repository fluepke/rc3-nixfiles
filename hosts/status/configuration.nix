# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, lib, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ../../configuration/common
      ../../configuration/hcloud.nix
      ../../configuration/fde.nix
      ../../configuration/status
    ];

  networking.hostName = "status";
  networking.domain = "c3events.de";

  networking.interfaces.ens3.ipv4.addresses = [
    { address = "135.181.148.139"; prefixLength = 32; }
  ];
  networking.interfaces.ens3.useDHCP = true;
  networking.defaultGateway = {
    address = "172.31.1.1";
    interface = "ens3";
  };

  networking.interfaces.ens3.ipv6.addresses = [
    { address = "2a01:4f9:c010:e505::1"; prefixLength = 64; }
  ];
  networking.defaultGateway6 = { address = "fe80::1"; interface = "ens3"; };

  rc3.deploy.ssh.host = "135.181.148.139";

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.03"; # Did you read the comment?

}


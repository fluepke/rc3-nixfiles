{ pkgs, lib, ... }:

let
#  nur-no-pkgs = import (builtins.fetchTarball ("https://github.com/nix-community/NUR/archive/master.tar.gz")) { inherit pkgs; };
in {
  imports = [
#    nur-no-pkgs.repos.kolloch.modules.jitsi
    ./turn.nix
    ./prosody.nix
  ];

#  nixpkgs.config.packageOverrides = pkgs: {
#    inherit (nur-no-pkgs.repos.kolloch) jitsi-meet jitsi-videobridge jicofo;
#  };

  services.jitsi-videobridge.openFirewall = true;
  services.jitsi-meet = {
    enable = true;
    hostName = "jitsi.monitoring.rc3.world";
    config = {
      useIPv6 = true;
      useStunTurn = true;
      startAudioOnly = true;
      desktopSharingFirefoxDisabled = true;
      desktopSharingFrameRate.min = 5;
      desktopSharingFrameRate.max = 10;
      disableThirdPartyRequests = true;
      p2p = {
        useStunTurn = true;
        stunServers = [
          { urls = "stun:jitsi.monitoring.rc3.world:3478"; }
          { urls = "stun:jitsi.monitoring.rc3.world:3479"; }
        ];
      };
    };
  };

  services.nginx.virtualHosts."jitsi.monitoring.rc3.world" = {
    enableACME = true;
    forceSSL   = true;
  };
}

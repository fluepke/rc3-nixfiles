{ lib, pkgs, config, ... }:

{
  users.users.prosody.extraGroups = [ "keys" "secrets" ];

  networking.firewall.allowedTCPPorts = [
    5269 /* s2s */
    5280 /* http */
    5281 /* https */
    5222 /* c2s */
    5223 /* c2s legacy ssl */
    5000 /* proxy65 */
  ];

  security.acme.certs."prosody" = {
    domain = "monitoring.rc3.world";
    # user = "nginx";
    group = "prosody";
    # allowKeysForGroup = true;
    webroot = config.security.acme.certs."jitsi.monitoring.rc3.world".webroot;
    extraDomains = {
      "jitsi.monitoring.rc3.world" = null;
    };
    postRun = "systemctl restart prosody";
  };

  services.prosody = {
    enable = true;
    dataDir = "/var/lib/prosody";
    admins = [ "hostmaster@jitsi.monitoring.rc3.world" ];
    allowRegistration = true;

    modules = {
      watchregistrations = true;
      server_contact_info = true;
    };

    package = (pkgs.prosody.overrideAttrs (oldAttrs: {
      communityModules = pkgs.fetchhg {
        url = "https://hg.prosody.im/prosody-modules";
        rev = "362997ededb1";
        sha256 = "1xkzszhzbyzw6q4prcvs164blm6k87bhn12zm4cwwbv9cd0fbhkv";
      };
    })).override {
      withCommunityModules = [
        "turncredentials"
        "prometheus"
        "measure_client_features"
        "measure_client_identities"
        "measure_client_presence"
        "measure_cpu"
        "measure_memory"
        "measure_message_e2ee"
        "measure_stanza_counts"
        "csi_battery_saver"
      ];
    };

    ssl.key = "/var/lib/acme/prosody/key.pem";
    ssl.cert = "/var/lib/acme/prosody/fullchain.pem";
    virtualHosts = {
      "monitoring.rc3.world" = {
        domain = "monitoring.rc3.world";
        enabled = true;
      };
    };

    # uploadHttp = { domain = "uploads.fluep.ke"; };

    extraConfig = ''
      statistics = "internal"
      statistics_interval = 5 -- in seconds

      legacy_ssl_ports = { 5223 }

      contact_info = {
        abuse = { "mailto:xmpp-abuse@jitsi.monitoring.rc3.world" };
        admin = { "mailto:xmpp-admin@jitsi.monitoring.rc3.world" };
        feedback = { "https://fluep.ke/", "mailto:xmpp-feedback@jitsi.monitoring.rc3.world" };
        security = { "mailto:security@jtsi.monitoring.rc3.world" };
      }

      turncredentials_host = "jitsi.monitoring.rc3.world"
      Include "/var/src/secrets/turn/turn.lua"
    '';

    muc = [
      {
        domain = "rooms.fluep.ke";
        restrictRoomCreation = "local";
      }
    ];
  };

  services.nginx.virtualHosts."${config.networking.hostName}.${config.networking.domain}" = {
    locations."/prosody-exporter/metrics".proxyPass = "http://${config.networking.hostName}.${config.networking.domain}:5280/metrics";
    locations."/prosody-exporter/metrics".extraConfig = config.services.nginx.virtualHosts."${config.networking.hostName}.${config.networking.domain}".locations."/node-exporter/metrics".extraConfig;
  };
}

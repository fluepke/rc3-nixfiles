{ ... }:

{
  imports = [
    ./hardware-configuration.nix
    ../../configuration/common
    ../../configuration/hcloud.nix
    ../../configuration/fde.nix
    ../../configuration/monitoring
  ];

  networking.hostName = "monitoring";
  networking.domain = "rc3.world";

  networking.interfaces.enp1s0.ipv4.addresses = [
    { address = "195.201.239.171"; prefixLength = 32; }
  ];
  networking.interfaces.enp1s0.useDHCP = true;
  networking.defaultGateway = {
    address = "172.31.1.1";
    interface = "enp1s0";
  };

  networking.interfaces.enp1s0.ipv6.addresses = [
    { address = "2a01:4f8:c010:19ad::1"; prefixLength = 64; }
  ];
  networking.defaultGateway6 = { address = "fe80::1"; interface = "enp1s0"; };

  rc3.deploy.ssh.host = "195.201.239.171";

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.03"; # Did you read the comment?
}

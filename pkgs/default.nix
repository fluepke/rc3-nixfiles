{ pkgs, ... }:

let
  callPackage = pkgs.lib.callPackageWith(pkgs // custom);
  custom = {
    cachet = callPackage ./cachet {};
    gnmi-gateway = callPackage ./gnmi-gateway {};
    prometheus-smokeping-exporter = callPackage ./prometheus-smokeping-exporter {};
    powerdns-manager = callPackage ./powerdns-manager {};
    powerdns-exporter = callPackage ./powerdns-exporter {};
    powerdns-auth-proxy = pkgs.python3.pkgs.callPackage ./powerdns-auth-proxy {};
    prometheus-junos-exporter = callPackage ./prometheus-junos-exporter {};
  };
in custom

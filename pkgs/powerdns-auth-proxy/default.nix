{ lib, python3, buildPythonPackage, ... }:

buildPythonPackage rec {
  pname = "powerdns-auth-proxy";
  version = "master";

  src = fetchGit {
    url = "https://github.com/catalyst/powerdns-auth-proxy.git";
    rev = "1ed4cdc960e4d2a344be098d88dbe271b22fecb3";
  };

  propagatedBuildInputs = [
    python3.pkgs.pytest
    python3.pkgs.pytest-flask
    python3.pkgs.requests
  ];

  meta = with lib; {
    homepage = "https://github.com/catalyst/powerdns-auth-proxy";
  };
}

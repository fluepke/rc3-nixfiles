{ stdenv, fetchurl, php }:

stdenv.mkDerivation rec {
  version = "2.0.1";
  pname = "powerdns-manager";
  src = fetchurl {
    url = "https://dl.pdnsmanager.org/pdnsmanager-${version}.tar.gz";
    sha256 = "08ch8k1gzaw7zydvh2plxvafqq4xjr714fa79ghvlid82lhqglhv";
  };
  patches = [ ./0001-hardcoded-database.patch ];
  installPhase = ''
    mkdir $out;
    cp -r * $out;
    echo -e "<?php\nreturn [ ];" > $out/backend/config/ConfigUser.php;
  '';
}

# file generated from go.mod using vgo2nix (https://github.com/nix-community/vgo2nix)
[
  {
    goPackagePath = "github.com/alecthomas/template";
    fetch = {
      type = "git";
      url = "https://github.com/alecthomas/template";
      rev = "fb15b899a751";
      sha256 = "1vlasv4dgycydh5wx6jdcvz40zdv90zz1h7836z7lhsi2ymvii26";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/alecthomas/units";
    fetch = {
      type = "git";
      url = "https://github.com/alecthomas/units";
      rev = "c3de453c63f4";
      sha256 = "0js37zlgv37y61j4a2d46jh72xm5kxmpaiw0ya9v944bjpc386my";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/beorn7/perks";
    fetch = {
      type = "git";
      url = "https://github.com/beorn7/perks";
      rev = "v1.0.1";
      sha256 = "17n4yygjxa6p499dj3yaqzfww2g7528165cl13haj97hlx94dgl7";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/cespare/xxhash/v2";
    fetch = {
      type = "git";
      url = "https://github.com/cespare/xxhash";
      rev = "v2.1.1";
      sha256 = "0rl5rs8546zj1vzggv38w93wx0b5dvav7yy5hzxa8kw7iikv1cgr";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/davecgh/go-spew";
    fetch = {
      type = "git";
      url = "https://github.com/davecgh/go-spew";
      rev = "v1.1.1";
      sha256 = "0hka6hmyvp701adzag2g26cxdj47g21x6jz4sc6jjz1mn59d474y";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/go-kit/kit";
    fetch = {
      type = "git";
      url = "https://github.com/go-kit/kit";
      rev = "v0.9.0";
      sha256 = "09038mnw705h7isbjp8dzgp2i04bp5rqkmifxvwc5xkh75s00qpw";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/go-logfmt/logfmt";
    fetch = {
      type = "git";
      url = "https://github.com/go-logfmt/logfmt";
      rev = "v0.4.0";
      sha256 = "06smxc112xmixz78nyvk3b2hmc7wasf2sl5vxj1xz62kqcq9lzm9";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/go-stack/stack";
    fetch = {
      type = "git";
      url = "https://github.com/go-stack/stack";
      rev = "v1.8.0";
      sha256 = "0wk25751ryyvxclyp8jdk5c3ar0cmfr8lrjb66qbg4808x66b96v";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/gogo/protobuf";
    fetch = {
      type = "git";
      url = "https://github.com/gogo/protobuf";
      rev = "v1.1.1";
      sha256 = "1525pq7r6h3s8dncvq8gxi893p2nq8dxpzvq0nfl5b4p6mq0v1c2";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/golang/protobuf";
    fetch = {
      type = "git";
      url = "https://github.com/golang/protobuf";
      rev = "v1.4.0";
      sha256 = "1fjvl5n77abxz5qsd4mgyvjq19x43c5bfvmq62mq3m5plx6zksc8";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/google/go-cmp";
    fetch = {
      type = "git";
      url = "https://github.com/google/go-cmp";
      rev = "v0.4.0";
      sha256 = "1x5pvl3fb5sbyng7i34431xycnhmx8xx94gq2n19g6p0vz68z2v2";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/google/gofuzz";
    fetch = {
      type = "git";
      url = "https://github.com/google/gofuzz";
      rev = "v1.0.0";
      sha256 = "0qz439qvccm91w0mmjz4fqgx48clxdwagkvvx89cr43q1d4iry36";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/json-iterator/go";
    fetch = {
      type = "git";
      url = "https://github.com/json-iterator/go";
      rev = "v1.1.9";
      sha256 = "0pkn2maymgl9v6vmq9q1si8xr5bbl88n6981y0lx09px6qxb29qx";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/julienschmidt/httprouter";
    fetch = {
      type = "git";
      url = "https://github.com/julienschmidt/httprouter";
      rev = "v1.2.0";
      sha256 = "1k8bylc9s4vpvf5xhqh9h246dl1snxrzzz0614zz88cdh8yzs666";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/konsorten/go-windows-terminal-sequences";
    fetch = {
      type = "git";
      url = "https://github.com/konsorten/go-windows-terminal-sequences";
      rev = "v1.0.1";
      sha256 = "1lchgf27n276vma6iyxa0v1xds68n2g8lih5lavqnx5x6q5pw2ip";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/kr/logfmt";
    fetch = {
      type = "git";
      url = "https://github.com/kr/logfmt";
      rev = "b84e30acd515";
      sha256 = "02ldzxgznrfdzvghfraslhgp19la1fczcbzh7wm2zdc6lmpd1qq9";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/kr/pretty";
    fetch = {
      type = "git";
      url = "https://github.com/kr/pretty";
      rev = "v0.1.0";
      sha256 = "18m4pwg2abd0j9cn5v3k2ksk9ig4vlwxmlw9rrglanziv9l967qp";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/kr/pty";
    fetch = {
      type = "git";
      url = "https://github.com/kr/pty";
      rev = "v1.1.1";
      sha256 = "0383f0mb9kqjvncqrfpidsf8y6ns5zlrc91c6a74xpyxjwvzl2y6";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/kr/text";
    fetch = {
      type = "git";
      url = "https://github.com/kr/text";
      rev = "v0.1.0";
      sha256 = "1gm5bsl01apvc84bw06hasawyqm4q84vx1pm32wr9jnd7a8vjgj1";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/matttproud/golang_protobuf_extensions";
    fetch = {
      type = "git";
      url = "https://github.com/matttproud/golang_protobuf_extensions";
      rev = "v1.0.1";
      sha256 = "1d0c1isd2lk9pnfq2nk0aih356j30k3h1gi2w0ixsivi5csl7jya";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/modern-go/concurrent";
    fetch = {
      type = "git";
      url = "https://github.com/modern-go/concurrent";
      rev = "bacd9c7ef1dd";
      sha256 = "0s0fxccsyb8icjmiym5k7prcqx36hvgdwl588y0491gi18k5i4zs";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/modern-go/reflect2";
    fetch = {
      type = "git";
      url = "https://github.com/modern-go/reflect2";
      rev = "v1.0.1";
      sha256 = "06a3sablw53n1dqqbr2f53jyksbxdmmk8axaas4yvnhyfi55k4lf";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/mwitkow/go-conntrack";
    fetch = {
      type = "git";
      url = "https://github.com/mwitkow/go-conntrack";
      rev = "cc309e4a2223";
      sha256 = "0nbrnpk7bkmqg9mzwsxlm0y8m7s9qd9phr1q30qlx2qmdmz7c1mf";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/pkg/errors";
    fetch = {
      type = "git";
      url = "https://github.com/pkg/errors";
      rev = "v0.9.1";
      sha256 = "1761pybhc2kqr6v5fm8faj08x9bql8427yqg6vnfv6nhrasx1mwq";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/pmezard/go-difflib";
    fetch = {
      type = "git";
      url = "https://github.com/pmezard/go-difflib";
      rev = "v1.0.0";
      sha256 = "0c1cn55m4rypmscgf0rrb88pn58j3ysvc2d0432dp3c6fqg6cnzw";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/prometheus/client_golang";
    fetch = {
      type = "git";
      url = "https://github.com/prometheus/client_golang";
      rev = "v1.6.0";
      sha256 = "0wwkx69in9dy5kzd3z6rrqf5by8cwl9r7r17fswcpx9rl3g61x1l";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/prometheus/client_model";
    fetch = {
      type = "git";
      url = "https://github.com/prometheus/client_model";
      rev = "v0.2.0";
      sha256 = "0jffnz94d6ff39fr96b5w8i8yk26pwnrfggzz8jhi8k0yihg2c9d";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/prometheus/common";
    fetch = {
      type = "git";
      url = "https://github.com/prometheus/common";
      rev = "v0.10.0";
      sha256 = "1ra7zfqsnvgfizzsxn3rk0dwhk7nx8p8082zgpvadbv2b0j1k01i";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/prometheus/procfs";
    fetch = {
      type = "git";
      url = "https://github.com/prometheus/procfs";
      rev = "v0.0.11";
      sha256 = "1msc8bfywsmrgr2ryqjdqwkxiz1ll08r3qgvaka2507z1wpcpj2c";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/sirupsen/logrus";
    fetch = {
      type = "git";
      url = "https://github.com/sirupsen/logrus";
      rev = "v1.4.2";
      sha256 = "087k2lxrr9p9dh68yw71d05h5g9p5v26zbwd6j7lghinjfaw334x";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/stretchr/objx";
    fetch = {
      type = "git";
      url = "https://github.com/stretchr/objx";
      rev = "v0.1.1";
      sha256 = "0iph0qmpyqg4kwv8jsx6a56a7hhqq8swrazv40ycxk9rzr0s8yls";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "github.com/stretchr/testify";
    fetch = {
      type = "git";
      url = "https://github.com/stretchr/testify";
      rev = "v1.6.0";
      sha256 = "1xfrz8hwp76f8a7z0b55pzjdb3a4hh4c45sz49zmpnq8b3lfb39h";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/crypto";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/crypto";
      rev = "06a226fb4e37";
      sha256 = "0fdig6jx81g7a44dnxggibl909wchsj4nakmmhhz7db36sl0d7m5";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/net";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/net";
      rev = "d28f0bde5980";
      sha256 = "18xj31h70m7xxb7gc86n9i21w6d7djbjz67zfaljm4jqskz6hxkf";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/sync";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/sync";
      rev = "cd5d95a43a6e";
      sha256 = "1nqkyz2y1qvqcma52ijh02s8aiqmkfb95j08f6zcjhbga3ds6hds";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/sys";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/sys";
      rev = "1957bb5e6d1f";
      sha256 = "0imqk4l9785rw7ddvywyf8zn7k3ga6f17ky8rmf8wrri7nknr03f";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/text";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/text";
      rev = "v0.3.0";
      sha256 = "0r6x6zjzhr8ksqlpiwm5gdd7s209kwk5p4lw54xjvz10cs3qlq19";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "golang.org/x/xerrors";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/xerrors";
      rev = "9bdfabe68543";
      sha256 = "1yjfi1bk9xb81lqn85nnm13zz725wazvrx3b50hx19qmwg7a4b0c";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "google.golang.org/protobuf";
    fetch = {
      type = "git";
      url = "https://go.googlesource.com/protobuf";
      rev = "v1.21.0";
      sha256 = "12bwln8z1lf9105gdp6ip0rx741i4yfz1520gxnp8861lh9wcl63";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "gopkg.in/alecthomas/kingpin.v2";
    fetch = {
      type = "git";
      url = "https://gopkg.in/alecthomas/kingpin.v2";
      rev = "v2.2.6";
      sha256 = "0mndnv3hdngr3bxp7yxfd47cas4prv98sqw534mx7vp38gd88n5r";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "gopkg.in/check.v1";
    fetch = {
      type = "git";
      url = "https://gopkg.in/check.v1";
      rev = "41f04d3bba15";
      sha256 = "0vfk9czmlxmp6wndq8k17rhnjxal764mxfhrccza7nwlia760pjy";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "gopkg.in/yaml.v2";
    fetch = {
      type = "git";
      url = "https://gopkg.in/yaml.v2";
      rev = "v2.3.0";
      sha256 = "1md0hlyd9s6myv3663i9l59y74n4xjazifmmyxn43g86fgkc5lzj";
      moduleDir = "";
    };
  }
  {
    goPackagePath = "gopkg.in/yaml.v3";
    fetch = {
      type = "git";
      url = "https://gopkg.in/yaml.v3";
      rev = "9f266ea9e77c";
      sha256 = "1bbai3lzb50m0x2vwsdbagrbhvfylj9k1m32hgbqwldqx4p9ay35";
      moduleDir = "";
    };
  }
]

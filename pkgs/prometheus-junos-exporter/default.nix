{ stdenv, buildGoPackage, fetchFromGitHub, nixosTests }:

buildGoPackage rec {
  pname = "prometheus-junos-exporter";
  version = "0.9.7";

  src = fetchFromGitHub {
    owner = "czerwonk";
    repo = "junos_exporter";
    sha256 = "07yc4wwka9v327gnxhwv0d8sj06p0xyd3ynl70p65gb6fwsbsh6p";
    rev = "0.9.7";
  };

  goPackagePath = "github.com/czerwonk/junos_exporter";

  goDeps = ./deps.nix;

  meta = with stdenv.lib; {
    description = "Exporter for metrics from devices running JunOS (via SSH)";
    homepage = "https://github.com/czerwonk/junos_exporter";
    license = licenses.mit;
    maintainers = with maintainers; [ fluepke ];
    platforms = platforms.unix;
  };
}

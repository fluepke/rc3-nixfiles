# rC3 Nixfiles

> Notes for handling NixOS systems:
> 
> Do not attempt to edit files in `/etc` unless you know exactly what you are doing. Your changes _will_ be overwritten. All changes must be done in this repository and then deployed as explained below.
> SSH Port is 42023, on port 22 a tarssh slowly welcomes you.

### Setting up a NixOS on the Hetzner Cloud
Build a kexec tarball. Copy the resulting tarball to the machine and kexec:
```bash
nix build -f . kexec_tarball
pv ./result/tarball/nixos-system-x86_64-linux.tar.xz | ssh -o UserKnownHostsFile=/dev/null root@$IP "tar -xJC / && /kexec_nixos"
```

The machine will reboot into a pre-configured ramdisk with all you need to setup a NixOS installation.
Perform the usual NixOS [installation procedure](https://nixos.org/manual/nixos/unstable/index.html#sec-installation-partitioning).

> Note: For full disk encrypted systems additonal steps are required and `initrd.networking` has to be configured
In short form, it's this:

> Different Hetzner VMs have different *predictable* (haahaaa) device names for the primary NIC!

```
sudo sgdisk \
    -o \
    -n 1::+1M \
    -n 2::+512M \
    -n 3:: \
    -t 1:ef02 \
    /dev/sda

sudo mkfs.ext2 /dev/sda2
sudo cryptsetup luksFormat /dev/sda3  # append options as required by paranoia level
sudo cryptsetup luksOpen /dev/sda3 crypto
sudo mkfs.xfs -m reflink=1 /dev/mapper/crypto
sudo mount /dev/mapper/crypto /mnt
sudo mkdir /mnt/boot
sudo mount /dev/sda2 /mnt/boot

sudo nixos-generate-config --root /mnt

# copy hetzner-minimal.nix to /mnt/etc/nixos/configuration.nix

sudo mkdir -p /var/src/secrets/initrd /mnt/var/src/secrets/initrd
sudo ssh-keygen -t ed25519 -f /var/src/secrets/initrd/ed25519-hostkey
sudo cp /var/src/secrets/initrd/ed25519-hostkey /mnt/var/src/secrets/initrd/ed25519-hostkey

sudo nixos-install
sudo reboot
```

### Unlocking disk
To unlock the disk, `ssh` onto port **2222**, user is root.
If you are in the group `wheel`, your SSH pubkey is deployed.

### Building and deploying the configuration
```
nix build -f . deploy.ns1 && ./result switch # deploy ns1 host
nix build -f . deploy.dns && ./result switch # deploy dns group
```

## DNS
For some reason maintaining DNS zonefiles in a git repository or even better: creating them with Nix Expressions was considered bad, thus we have a [Web Frontend](https://dns-mgmt.rc3.world) and a [PowerDNS API](https://api.dns-mgmt.rc3.world) which is using a [PowerDNS Auth Proxy](https://github.com/catalyst/powerdns-auth-proxy) for ACLs.


## Monitoring
### Adding scrape targets

Some scrape targets are retrieved from netbox.
Adding `blackbox_icmp` and/or `blackbox_https` to a virtual machine or device will cause a blackbox exporter probe.

{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.rc3.dns;

in {
  options = {
    rc3.dns = {
      enable = mkEnableOption "";
      isPrimary = mkOption {
        type = types.bool;
        default = false;
        description = "Primary DNS server";
      };
      primaryIPs = mkOption {
        type = with types; listOf str;
        description = "Primary DNS server IP addresses";
      };
      primaryFqdn = mkOption {
        type = types.str;
        description = "Primary DNS server FQDN";
      };
      secondaryIPs = mkOption {
        type = with types; listOf str;
        description = "Secondary DNS server IP addresses";
      };
    };
  };

  config = mkIf cfg.enable {
    networking.firewall.allowedUDPPorts = [ 53 ];
    networking.firewall.allowedTCPPorts = [ 53 ];

    services.powerdns-exporter.enable = true;
    rc3.powerdns = let
      mkSupermaster = ip: { fqdn = cfg.primaryFqdn; inherit ip; };
    in
      {
        enable = true;
        allowAxfrIps = [ "::/0" "0.0.0.0/0" ];
        allowNotifyFrom = cfg.primaryIPs;
        alsoNotify = if cfg.isPrimary then cfg.secondaryIPs else [];
        master = cfg.isPrimary;
        supermasters = forEach cfg.primaryIPs (ip: mkSupermaster ip);
      };
    rc3.powerdns-manager.enable = cfg.isPrimary;
    rc3.powerdns-auth-proxy.enable = cfg.isPrimary;
  };
}

{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.rc3.powerdns-manager;
  fpm = config.services.phpfpm.pools.powerdns-manager;
  phpPackage =
    let
      base = pkgs.php;
    in
      base.buildEnv {
        extensions = { enabled, all }: with all;
          enabled ++ [
            apcu json pdo_mysql
          ];
      };
in
{
  options = {
    rc3.powerdns-manager = {
      enable = mkEnableOption "powerdns-manager";
      fqdn = mkOption {
        type = types.str;
        description = "FQDN to host powerdns-manager at";
        default = "${config.networking.hostName}.${config.networking.domain}";
      };
      adminPasswordFile = mkOption {
        type = types.str;
        description = "File that contains the admin password without trailing newline";
      };
      poolSettings = mkOption {
        type = with types; attrsOf (oneOf [ str int bool ]);
        default = {
          "pm" = "dynamic";
          "pm.max_children" = "32";
          "pm.start_servers" = "2";
          "pm.min_spare_servers" = "2";
          "pm.max_spare_servers" = "4";
          "pm.max_requests" = "500";
        };
      };
    };
  };

  config = mkIf cfg.enable {
    services.phpfpm = {
      pools.powerdns-manager = {
        user = "powerdns";
        group = "powerdns";
        settings = mapAttrs (name: mkDefault) {
          "listen.owner" = config.services.nginx.user;
          "listen.group" = config.services.nginx.group;
        } // cfg.poolSettings;
        inherit phpPackage;
      };
    };

    systemd.services.provision-powerdns-manager-users = {
      description = "Ensures users for powerdns-manager";
      wantedBy = [ "multi-user.target" ];
      after = [ "mysql.service" "powerdns-authoritative" ];
      serviceConfig = {
        Type = "oneshot";
        User = "powerdns";
        Group = "powerdns";
      };
      script = ''
        if [ -f  ${cfg.adminPasswordFile} ]; then
            export PASSWORD=$(cat ${cfg.adminPasswordFile})
            HASHED_PASSWORD=$(${pkgs.php}/bin/php -r 'echo password_hash(getenv("PASSWORD"), PASSWORD_DEFAULT);')
            echo "DELETE FROM users where name='admin'; INSERT INTO users (name, backend, type, password) VALUES ('admin', 'native', 'admin', '$HASHED_PASSWORD')" | ${pkgs.mysql}/bin/mysql --user="powerdns" "powerdns"
        else
            echo "Password file does not exist."
            exit 1
        fi
      '';
    };
    
    services.nginx.virtualHosts.${cfg.fqdn} = {
      root = "${pkgs.powerdns-manager}/backend/public";
      extraConfig = ''
        index index.html index.php;
      '';
      locations = {
        "/" = {
          root = "${pkgs.powerdns-manager}/frontend";
          extraConfig = ''
            try_files $uri $uri/ /index.html;
          '';
        };
        "/api" = {
          extraConfig = ''
            try_files $uri $uri/ /index.php;
          '';
        };
        "~* \.php$" = {
          extraConfig = ''
            if ($request_uri ~* "/api(.*)") {
                set $req $1;
            }
            fastcgi_split_path_info ^(/api)(/.*)$;
            fastcgi_pass     unix:${fpm.socket};
            include ${config.services.nginx.package}/conf/fastcgi.conf;
            fastcgi_param    SCRIPT_FILENAME $request_filename;
            fastcgi_param    REQUEST_URI $req;
            fastcgi_read_timeout 900;
          '';
        };
      };
    };    
  };
}

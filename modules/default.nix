{ ... }:

{
  imports = [
    ./alertmanager-bot
    ./cachet
    ./deploy
    ./dns
    ./powerdns
    ./powerdns-exporter
    ./powerdns-manager
    ./powerdns-auth-proxy
    ./sources
    ./tarssh
    ./monitoring
    ./prometheus-smokeping-exporter
    ./prometheus-junos-exporter
  ];
}

{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.powerdns-exporter;
  fqdn = with config.networking; hostName + "." + domain;

in {
  options = {

    services.powerdns-exporter = {

      enable = mkEnableOption "Enable the PowerDNS Exporter";

      powerdnsSocket = mkOption {
        type = types.str;
        default = "/var/run/powerdns/pdns.controlsocket";
      };

      listenAddress = mkOption {
        type = types.str;
        description = "IP and port to bind to";
        default = "[::1]:9120";
      };
    };

  };

  config = mkIf cfg.enable {
    environment.systemPackages = [ pkgs.powerdns-exporter ];

    systemd.services.powerdns-exporter = {
        description = "PowerDNS Exporter";
        documentation = [ "https://github.com/wrouesnel/pdns_exporter" ];
        wantedBy = [ "multi-user.target" ];
        after = [ "network.target" ];
        serviceConfig = {
          Restart = mkDefault "always";
          User = "powerdns";
          Group = "powerdns";
        };
        script = ''
          ${pkgs.powerdns-exporter}/bin/pdns_exporter \
           -collector.powerdns.socket="authoritative:unix:${cfg.powerdnsSocket}" \
           -collector.rate-limit.min-cooldown=3s \
           -web.listen-address="${cfg.listenAddress}"
         '';
    };
    services.nginx.virtualHosts.${fqdn} = {
      locations."/powerdns-exporter/metrics".proxyPass = "http://${cfg.listenAddress}/metrics";
    };
  };
}

{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.tarssh;

in {
  options = {

    services.tarssh = {

      enable = mkEnableOption "Enable the SSH tarpit server";

      delay = mkOption {
        type = types.int;
        description = "Seconds between responses";
        default = 10;
      };

      listenAddresses = mkOption {
        type = with types; listOf str;
        description = "Listen address(es) to bind to";
        default = [ "[::]" ];
      };

      listenPort = mkOption {
        type = types.int;
        description = "Port to bind to";
        default = 2222;
      };

      openFirewall = mkOption {
        type = types.bool;
        description = "Open port in firewall for incoming connections";
        default = true;
      };

      maxClients = mkOption {
        type = types.int;
        description = "Best-effort connection limit";
        default = 4096;
      };

      timeout = mkOption {
        type = types.int;
        description = "Socket write timeout";
        default = 30;
      };

    };

  };

  config = mkIf cfg.enable {
    networking.firewall.allowedTCPPorts = mkIf cfg.openFirewall [ cfg.listenPort ];

    environment.systemPackages = [ pkgs.tarssh ];

    systemd.services.tarssh = 
      let
        listenAddresses = concatStringsSep " " (map (
          addr: "${addr}:${toString cfg.listenPort}"
        ) cfg.listenAddresses);
      in {
        description = "SSH tarpit server";
        documentation = [ "https://github.com/Freaky/tarssh" ];
        wantedBy = [ "multi-user.target" ];
        after = [ "network.target" ];
        serviceConfig = {
          Restart = mkDefault "always";
          DynamicUser = "yes";
          ProtectSystem = "full";
          AmbientCapabilities = "CAP_NET_BIND_SERVICE";
        };
        script = ''
          ${pkgs.tarssh}/bin/tarssh \
           --delay ${toString cfg.delay} \
           --listen ${listenAddresses} \
           --max-clients ${toString cfg.maxClients} \
           --timeout ${toString cfg.timeout}
         '';
      };
    };
  }

{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.rc3.powerdns;
  supermasterOpts = { ... }:
    {
      options = {
        ip = mkOption {
          type = types.str;
          description = "Supermaster IP address";
        };
        fqdn = mkOption {
          type = types.str;
          description = "Supermaster FQDN";
        };
      };
    };

in {
  options = {
    rc3.powerdns = {
      enable = mkEnableOption "PowerDNS authoritative";
      allowAxfrIps = mkOption {
        type = with types; listOf str;
        description = "If set, only these IP addresses or netmasks will be able to perform AXFR without TSIG";
        default = [ "127.0.0.0/8" "::1" ];
      };
      allowDnsupdateFrom = mkOption {
        type = with types; listOf str;
        description = "Allow DNS updates from these IP ranges. Set to empty string to honour ALLOW-DNSUPDATE-FROM in ALLOW-DNSUPDATE-FROM";
        default = [ "127.0.0.0/8" "::1" ];
      };
      allowNotifyFrom = mkOption {
        type = with types; listOf str;
        description = "Allow AXFR NOTIFY from these IP ranges. Setting this to an empty string will drop all incoming notifies.";
        default = [ "127.0.0.0/8" "::1" ];
      };
      alsoNotify = mkOption {
        type = with types; listOf str;
        description = "When notifying a domain, also notify these nameservers";
        default = [];
      };
      apiKeyFile = mkOption {
        type = types.str;
        default = "/dev/null";
        description = "Path to a file containing the API Key";
      };
      localPort = mkOption {
        type = types.int;
        description = "Local port to bind to. If an address in local-address does not have an explicit port, this port is used.";
        default = 53;
      };
      localAddress = mkOption {
        type = with types; listOf str;
        description = "Local IP addresses to which we bind. Each address specified can include a port number; if no port is included then the local-port port will be used for that address. If a port number is specified, it must be separated from the address with a ‘:’; for an IPv6 address the address must be enclosed in square brackets.";
        default = [ "0.0.0.0" "[::]" ];
      };
      master = mkOption {
        type = types.bool;
        description = "Turn on master support.";
        default = false;
      };
      webserverAddress = mkOption {
        type = types.str;
        description = "IP Address for webserver/API to listen on";
        default = "[::1]";
      };
      webserverPort = mkOption {
        type = types.int;
        description = "Port of webserver/API to listen on";
        default = 8081;
      };

      supermasters = mkOption {
        type = with types; listOf (submodule supermasterOpts);
        description = "Supermasters";
        default = [];
      };

      schemaFile = mkOption {
        type = types.path;
        description = "SQL to use for initializing the database";
        default = ./setup.sql;
      };
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ mysql powerdns ];
    users.groups.powerdns = {};
    users.users.powerdns = {
      isSystemUser = true;
      group = "powerdns";
    };

    services.mysql.package = pkgs.mariadb;
    services.mysql = {
      enable = true;
      bind = mkDefault "127.0.0.1";
      ensureDatabases = [ "powerdns" ];
      ensureUsers = [
        {
          name = "powerdns";
          ensurePermissions."powerdns.*" = "ALL PRIVILEGES";
        }
      ];
    };    

    systemd.services.powerdns-authoritative = {
      description = "PowerDNS Authoritative DNS";
      documentation = [ "https://doc.powerdns.com/authoritative" ];
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      serviceConfig = {
        User = "powerdns";
        Group = "powerdns";
        WorkingDirectory = "/var/lib/powerdns";
        RuntimeDirectory = "powerdns";
        RuntimeDirectoryMode = "0700";
        StateDirectory = "powerdns";
        ExecStartPre = let
          pipeToMysql = ''${pkgs.mysql}/bin/mysql --user="powerdns" "powerdns"'';
          mkSupermaster = supermaster: "INSERT INTO supermasters VALUES ('${supermaster.ip}', '${supermaster.fqdn}', 'generatedByNix');";
          supermastersSql = concatStringsSep "\n" (forEach cfg.supermasters (supermaster: mkSupermaster supermaster));
          script = ''
            #!${pkgs.runtimeShell}
            cat ${cfg.schemaFile} | ${pipeToMysql}
          '' + (if cfg.master then "" else ''
            echo "TRUNCATE TABLE supermasters;
            ${supermastersSql}" | ${pipeToMysql}
          '');
        in (pkgs.bash + "/bin/bash " + (pkgs.writeText "powerdns-authoritative-start-pre" script));
        Restart = "always";
        AmbientCapabilities = "CAP_NET_BIND_SERVICE";
      };
      script = ''
        ${pkgs.powerdns}/bin/pdns_server \
          --allow-axfr-ips="${ concatStringsSep "," cfg.allowAxfrIps }" \
          --allow-notify-from="${ concatStringsSep "," (if cfg.master then [] else cfg.allowNotifyFrom) }" \
          --allow-dnsupdate-from="${ concatStringsSep "," cfg.allowDnsupdateFrom }" \
          --also-notify="${ concatStringsSep "," cfg.alsoNotify }" \
          --api=${ if cfg.master then "yes" else "no" } \
          --api-key=$(cat ${cfg.apiKeyFile}) \
          --default-soa-edit="DEFAULT" \
          --default-api-rectify \
          --gmysql-dnssec=yes \
          --gmysql-socket="/run/mysqld/mysqld.sock" \
          --launch="gmysql" \
          --local-address="${ concatStringsSep "," cfg.localAddress}" \
          --local-port=${toString cfg.localPort} \
          --master=${ if cfg.master then "yes" else "no" } \
          --no-config \
          --reuseport=yes \
          --slave=${ if cfg.master then "no" else "yes" } \
          --slave-cycle-interval=30 \
          --socket-dir=/var/run/powerdns \
          --superslave=${ if cfg.master then "no" else "yes" } \
          --udp-truncation-threshold=1232 \
          --version-string="Bind 42.0" \
          --webserver=yes \
          --webserver-address=${cfg.webserverAddress} \
          --webserver-password=$(cat ${cfg.apiKeyFile}) \
          --webserver-port=${toString cfg.webserverPort} \
          --webserver-print-arguments=no \
          --xfr-max-received-mbytes=1
      '';
    };
  };
}

{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.alertmanager-bot;
in {
  options = {
    services.alertmanager-bot = {
      enable = mkEnableOption "Enable Bot for Prometheus' Alertmanager";
      listenAddress = mkOption {
        type = types.str;
        description = "Address and port to bind to";
        default = "127.0.0.1:9103";
      };
      alertmanagerUrl = mkOption {
        type = types.str;
        description = "The URL that's used to connect to the alertmanager";
        default = "http://localhost:9093";
      };
      telegramAdmins = mkOption {
        type = with types; listOf str;
        description = "The ID of the initial Telegram Admin";
      };
      telegramTokenFile = mkOption {
        type = types.str;
        description = "The token used to connect with Telegram";
      };
      templatePaths = mkOption {
        type = with types; listOf path;
        description = "The paths to the template";
        default = [ ./default.tmpl ];
      };
      store = mkOption {
        type = types.enum [ "bolt" "consul" ];
        description = "The store to use";
        default = "bolt";
      };
      consulUrl = mkOption {
        type = types.str;
        description = "The URL that's used to connect to the consul store";
        default = "localhost:8500";
      };
      boltPath = mkOption {
        type = types.str;
        description = "The path to the file where bolt persists its data";
        default = "/var/lib/alertmanager-bot/bolt.db";
      };
    };
  };

  config = mkIf cfg.enable {
      environment.systemPackages = [ pkgs.alertmanager-bot ];
      systemd.services.alertmanager-bot =
        let
          paths = concatStringsSep " " cfg.templatePaths;
          admins = concatStringsSep " " (forEach cfg.telegramAdmins (
            admin: "--telegram.admin=${admin}"));
        in {
          description = "Bot for Prometheus' Alertmanager";
          documentation = [ "https://github.com/metalmatze/alertmanager-bot" ];
          wantedBy = [ "multi-user.target" ];
          after = [ "alertmanager.service" ];
          serviceConfig = {
            Restart = mkDefault "always";
            DynamicUser = "yes";
            ProtectSystem = "full";
            PrivateTmp = "true";
            StateDirectory = "alertmanager-bot";
          };
          script = ''
            ${pkgs.alertmanager-bot}/bin/alertmanager-bot \
              --listen.addr="${cfg.listenAddress}" \
              --consul.url="${cfg.consulUrl}" \
              --alertmanager.url="${cfg.alertmanagerUrl}" \
              --telegram.token="$(cat ${cfg.telegramTokenFile})" \
              --template.paths=${paths} \
              --store="${cfg.store}" \
              --bolt.path="${cfg.boltPath}" \
              ${admins}
          '';
         };  
  };
}

{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.rc3.powerdns-auth-proxy;
  fqdn = "api.${config.networking.hostName}.${config.networking.domain}";
  apiUserOpts = { ... }:
    {
      options = {
        keyFile = mkOption {
          type = types.str;
          description = "File containing the user API key";
        };
        allowSuffixCreation = mkOption {
          type = with types; listOf str;
          description = "List of zones user is allowed to manipulate";
        };
      };
    };
in {
  options = {
    rc3.powerdns-auth-proxy = {
      enable = mkEnableOption "";
      apiKeyFile = mkOption {
        type = types.str;
        default = config.rc3.powerdns.apiKeyFile;
        description = "Path to a file containing the API Key";
      };
      apiUsers = mkOption {
        type = with types; attrsOf (submodule apiUserOpts);
        default = {};
      };
      apiUrl = mkOption {
        type = types.str;
        default = "http://${config.rc3.powerdns.webserverAddress}:${toString config.rc3.powerdns.webserverPort}/api/v1/servers/localhost";
      };
      listenAddress = mkOption {
        type = types.str;
        default = "[::1]:8082";
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.services.powerdns-auth-proxy = let
      virtualenv = pkgs.python3.buildEnv.override {
        extraLibs = with pkgs.python3.pkgs; [
          flask
          requests
          pkgs.powerdns-auth-proxy
        ];
      };
    in {
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        User = "powerdns";
        Group = "powerdns";
        RuntimeDirectory = "powerdns-auth-proxy";
        WorkingDirectory = "/var/run/powerdns-auth-proxy";
        ExecStartPre = let
          apiUsersList = mapAttrsToList (name: value: ''
            [user:${name}]
            key = $(cat ${value.keyFile})
            allow-suffix-creation = ${concatStringsSep "," value.allowSuffixCreation}
          '') cfg.apiUsers;
          apiUsersCfg = concatStringsSep "\n" apiUsersList;
          script = ''
            #!${pkgs.runtimeShell}
            CONFIG_FILE=/var/run/powerdns-auth-proxy/proxy.ini
            echo "[pdns]" > $CONFIG_FILE
            echo "api-key = $(cat ${cfg.apiKeyFile})" >> $CONFIG_FILE
            echo "api-url = ${cfg.apiUrl}" >> $CONFIG_FILE
            echo "override-soa_edit_api = INCEPTION-INCREMENT" >> $CONFIG_FILE
            echo "override-nameservers = ns1.rc3.world. ns2.rc3.world." >> $CONFIG_FILE
            echo "override-kind = Master" >> $CONFIG_FILE
            echo "${apiUsersCfg}" >> $CONFIG_FILE
          '';
        in (pkgs.bash + "/bin/bash " + (pkgs.writeText "powerdns-auth-proxy-start-pre" script));

        ExecStart = ''
          /usr/bin/env waitress-serve \
            --threads=8 \
            --listen=${cfg.listenAddress} \
            --call 'powerdns_auth_proxy:create_app'
        '';
      };
      path = [
        pkgs.python3.pkgs.waitress
        virtualenv
      ];
      environment = {
        PYTHONPATH = "${virtualenv}/${pkgs.python3.sitePackages}";
      };
    };
    services.nginx.virtualHosts.${fqdn} = { 
      forceSSL = true; 
      enableACME = true; 
      locations."/".proxyPass = "http://${cfg.listenAddress}/"; 
    }; 

  };
}

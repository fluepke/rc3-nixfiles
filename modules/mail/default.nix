{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.rc3.mail;

  accountOpts = { ... }: {
    options = {
      name = mkOption {
        type = types.str;
        example = "chaos@rc3.world";
        description = "username";
      };
      hashedPassword = mkOption {
        type = types.str;
      };
      aliases = mkOption {
        type = with types; listOf str;
        example = [ "abuse@rc3.world" "postmaster@rc3.world" ];
      };
    };
  };

in {
  options = {

    rc3.mail = {

      enable = mkEnableOption "";
      fqdn = mkOption {
        type = types.str;
        default = "${config.networking.hostName}.${config.networking.domain}";
      };
      domains = mkOption {
        type = with types; listOf str;
        default = [ ];
      };
      accounts = mkOption {
        type = with types; attrsOf (submodule accountOpts);
        default = {};
      };
  };

  config = mkIf cfg.enable {
    networking.firewall.allowedTCPPorts = [ 25 587 ];
    services.dovecot2 = import ./dovecot2.nix { inherit pkgs cfg };
  };
}

{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.prometheus-junos-exporter;
in {
  options = {
    services.prometheus-junos-exporter = {
      enable = mkEnableOption "Enable prometheus junos exporter";
      user = mkOption {
        type = types.str;
        description = "User to run as";
        default = "prometheus-junos-exporter";
      };
      group = mkOption {
        type = types.str;
        description = "Group to run as";
        default = "prometheus-junos-exporter";
      };
      listenAddress = mkOption {
        type = types.str;
        description = "Address and port to bind to";
        default = "[::1]:9326";
      };
      telemetryPath = mkOption {
        type = types.str;
        description = "Path under which to expose metrics";
        default = "/metrics";
      };
      enabledCollectors = mkOption {
        type = with types; listOf str;
        description = "Enabled collectors";
        default = [ "alarm" "bgp" "environment" "firewall" "fpc" "ifdiag" "interfaces" "isis" "ldp" "ospf" "routes"
          "routingengine" "rpki" "storage" "system" ];
      };
      sshUser = mkOption {
        type = types.str;
        description = "SSH user";
        default = "monitoring";
      };
      sshKeyFile = mkOption {
        type = types.str;
        description = "SSH key file";
      };
      sshTargets = mkOption {
        type = with types; listOf str;
        description = "List of hosts to scrape";
        default = [ ];
      };
    };
  };

  config = mkIf cfg.enable {
      environment.systemPackages = [ pkgs.prometheus-junos-exporter ];
      users.groups.${cfg.group} = mkIf (cfg.group == "prometheus-junos-exporter") {};
      users.users.${cfg.user} = mkIf (cfg.user == "prometheus-junos-exporter") {
        isSystemUser = true;
        group = cfg.group;
      };
      systemd.services.prometheus-junos-exporter =
        let
          targets = concatStringsSep "," cfg.sshTargets;
          collectors = concatStringsSep "\\\n" (forEach cfg.enabledCollectors (collector: "  --${collector}.enabled "));
        in {
          description = "Exporter for metrics from devices running JunOS (via SSH)";
          documentation = [ "https://github.com/czerwonk/junos_exporter" ];
          wantedBy = [ "multi-user.target" ];
          after = [ "network.target" ];
          serviceConfig = {
            Restart = mkDefault "always";
            User = mkDefault cfg.user;
            Group = mkDefault cfg.group;
            ProtectSystem = "full";
          };
          script = ''
            ${pkgs.prometheus-junos-exporter}/bin/junos_exporter \
              --web.listen-address="${cfg.listenAddress}" \
              --web.telemetry-path="${cfg.telemetryPath}" \
              --ssh.user="${cfg.sshUser}" \
              --ssh.keyfile="${cfg.sshKeyFile}" \
              --ssh.targets="${targets}" \
              ${collectors}
          '';
         };
  };
}

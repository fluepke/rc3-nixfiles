{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.rc3.monitoring;
  fqdn = "${config.networking.hostName}.${config.networking.domain}";
in {
  options = {
    rc3.monitoring = {
      enable = mkEnableOption "Enable monitoring stack";
      promFqdn = mkOption {
        type = types.str;
        default = "prometheus.${fqdn}";
        description = "FQDN to expose prometheus at";
      };
      promLocation = mkOption {
        type = types.str;
        default = "/";
        description = "Path to expose prometheus at";
      };
      grafFqdn = mkOption {
        type = types.str;
        default = "grafana.${fqdn}";
        description = "FQDN to expose grafana at";
      };
      grafLocation = mkOption {
        type = types.str;
        default = "/";
        description = "Path to expose grafana at";
      };
      alertFqdn = mkOption {
        type = types.str;
        default = "alertmanager.${fqdn}";
        description = "FQDN to expose alertmanager at";
      };
      alertLocation = mkOption {
        type = types.str;
        default = "/";
        description = "Path to expose alertmanager at";
      };
      exporters = mkOption {
        type = with types; listOf str;
        default = [ "node_exporter" "wireguard_exporter" "jitsi_exporter" "jicofo_exporter" "nginx_exporter" "nginx_log_exporter" ];
        description = "Exporters to fetch their targets from netbox";
      };
    };
  };
  config = mkIf cfg.enable {
    services.prometheus = {
      enable = true;
      extraFlags = [ "--query.timeout=3s" ];
      globalConfig.scrape_interval = "15s";
      webExternalUrl = "https://${cfg.promFqdn}${cfg.promLocation}";
      listenAddress = "[::1]";
      scrapeConfigs = import ./scrape-configs.nix { inherit config lib pkgs; };
      # ruleFiles
      alertmanagers = [
        {
          scheme = "https";
          static_configs = [{ targets = [ cfg.alertFqdn ]; }];
          path_prefix = cfg.alertLocation;
          basic_auth = {
            username = "monitoring";
            password = "\${ALERTMANAGER}";
          };
        }
      ];
    };
    systemd.timers.prometheus-scrape-configs = {
      enable = true;
      description = "Fetches prometheus scrape configs from netbox inventory";
      timerConfig = {
        OnBootSec = "1min";
        OnUnitActiveSec = "15min";
      };
      wantedBy = [ "timers.target" ];
    };
    systemd.services.prometheus-scrape-configs = {
      enable = true;
      serviceConfig = {
        Type = "oneshot";
        User = "prometheus";
        Group = "prometheus";
        StateDirectory = "prometheus-scrape-configs";
      };
      description = "Fetches prometheus scrape configs from netbox inventory";
      script = let
        fetchScrapeTargetsPy = pkgs.writeText "fetchScrapeTargets.py" (builtins.readFile ./fetchScrapeTargets.py);
      in ''
        ${pkgs.python39}/bin/python ${fetchScrapeTargetsPy} \
          --netbox-token=$(cat /var/src/prometheus-scrape-configs/netbox.token) \
          --output-dir=/var/lib/prometheus-scrape-configs \
          --exporters ${concatStringsSep " " cfg.exporters }

          TARGETS=$(${pkgs.dnsutils}/bin/dig +noall +answer axfr visit.at.rc3.world @ns1.rc3.world | ${pkgs.gnugrep}/bin/grep 'IN\s\+A\s' | /run/current-system/sw/bin/awk '{ print $1 }' | /run/current-system/sw/bin/uniq)
          echo -n '[{ "targets": [' > /var/lib/prometheus-scrape-configs/workadventure.json
          while IFS= read -r line ; do echo -n "\""''${line:0:-1}"\"," >> /var/lib/prometheus-scrape-configs/workadventure.json ; done <<< $TARGETS
          echo '"rc3.world"]}]' >> /var/lib/prometheus-scrape-configs/workadventure.json

          TARGETS=$(${pkgs.dnsutils}/bin/dig +noall +answer axfr api.at.rc3.world @ns1.rc3.world | ${pkgs.gnugrep}/bin/grep 'IN\s\+A\s' | /run/current-system/sw/bin/awk '{ print $1 }' | /run/current-system/sw/bin/uniq)
          echo -n '[{ "targets": [' > /var/lib/prometheus-scrape-configs/workadventure-backend.json
          while IFS= read -r line ; do echo -n "\""''${line:0:-1}"\"," >> /var/lib/prometheus-scrape-configs/workadventure-backend.json ; done <<< $TARGETS
          echo '"rc3.world"]}]' >> /var/lib/prometheus-scrape-configs/workadventure-backend.json
      '';
    };

    systemd.services.grafana.serviceConfig.EnvironmentFile = "/var/src/secrets/grafana/env";

    services.grafana = {
      enable = true;
      addr = "[::1]";
      auth.anonymous.enable = true;
      auth.anonymous.org_name = "Public";
      rootUrl = "https://${cfg.grafFqdn}${cfg.grafLocation}";
      provision = {
        enable = true;
        datasources = [
          {
            basicAuth = true;
            basicAuthUser = "monitoring";
            basicAuthPassword = "$BASIC_AUTH_PASSWORD";
            type = "prometheus";
            name = "prometheus";
            url = "https://${cfg.promFqdn}${cfg.promLocation}";
            isDefault = true;
          }
        ];
        dashboards = [
          {
            options.path = ./dashboards;
          }
        ];
      };
    };

    services.prometheus.alertmanager = {
      enable = true;
      listenAddress = "[::1]";
      extraFlags = [
        ''--cluster.advertise-address="127.0.0.1:9093"'' #TODO fetch IPs from ifaces
      ];
      webExternalUrl = "https://${cfg.alertFqdn}${cfg.alertLocation}";
      configuration = {
        global = {
          smtp_from = "alertmanager@${fqdn}";
          smtp_hello = fqdn;
          resolve_timeout = "30s";
        };
        route = {
          receiver = "alertmanager-bot";
          group_wait = "30s";
          group_interval = "5m";
          repeat_interval = "24h";
          group_by = [ "alertname" "cluster" "site" ];
        };
        receivers = [
          {
            name = "alertmanager-bot";
            webhook_configs = [
              {
                send_resolved = true;
                url = "http://${config.services.alertmanager-bot.listenAddress}";
              }
            ];
          }
        ];
      };
    };

    services.prometheus-smokeping-exporter.enable = true;

    systemd.services.prometheus.serviceConfig.EnvironmentFile = [
      "/var/src/secrets/prometheus/scrape-secrets.env"
    ];

    services.nginx.virtualHosts = {
      ${cfg.promFqdn} = {
        enableACME = true;
        forceSSL = true;
        basicAuthFile = "/var/src/secrets/nginx/basic.auth";
        locations.${cfg.promLocation}.proxyPass = let
          addr = config.services.prometheus.listenAddress;
          port = toString config.services.prometheus.port;
        in "http://${addr}:${port}";
      };
      ${cfg.grafFqdn} = {
        enableACME = true;
        forceSSL = true;
        locations.${cfg.grafLocation}.proxyPass = let
          addr = config.services.grafana.addr;
          port = toString config.services.grafana.port;
        in "http://${addr}:${port}";
      };
      ${cfg.alertFqdn} = {
        enableACME = true;
        forceSSL = true;
        basicAuthFile = "/var/src/secrets/nginx/basic.auth";
        locations.${cfg.alertLocation}.proxyPass = let
          addr = config.services.prometheus.alertmanager.listenAddress;
          port = toString config.services.prometheus.alertmanager.port;
        in "http://${addr}:${port}";
      };
    };
  };
}

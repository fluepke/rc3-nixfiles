{ config, lib, pkgs, ... }:

with lib;

let
  inherit (import ../../lib/hosts.nix { inherit pkgs; }) hosts;
  mkTargets = targets: [{ inherit targets; }];
  fqdn = "${config.networking.hostName}.${config.networking.domain}";
  getFqdn = host: host.config.networking.hostName + "." + host.config.networking.domain;

  removePort = [
    {
      source_labels = [ "__address__" ];
      target_label = "instance";
      regex = "^(.*):?\\d*";
      action = "replace";
    }
  ];
  relabel_configs = [
    {
      source_labels = [ "__address__" ];
      target_label = "__param_target";
    }
    {
      source_labels = [ "__param_target" ];
      target_label = "instance";
    }
    {
      replacement = fqdn;
      target_label = "__address__";
    }
  ];


  dnsNodes = mapAttrsToList (name: host: getFqdn host) (filterAttrs (
    name: host: builtins.elem "dns" host.config.rc3.deploy.groups
  ) hosts);

  monitoringNodes = filterAttrs (
    name: host: builtins.elem "monitoring-cluster" host.config.rc3.deploy.groups
  ) hosts;

  smokepingNodes = mapAttrsToList (name: host: getFqdn host) monitoringNodes;

  prometheusNodes = mapAttrsToList (
    name: host: host.config.rc3.monitoring.promFqdn
  ) monitoringNodes;

  alertmanagerNodes = mapAttrsToList (
    name: host: host.config.rc3.monitoring.alertFqdn
  ) monitoringNodes;

  grafanaNodes = mapAttrsToList (
    name: host: host.config.rc3.monitoring.grafFqdn
  ) monitoringNodes;

  allNodes = mapAttrsToList (
    name: host: getFqdn host
  ) hosts;

  dnsTargets = mkTargets dnsNodes;
  dereferrerTargets = mkTargets [
    "node01.dereferrer.rc3.world:57923"
    "node02.dereferrer.rc3.world:57923"
    "node03.dereferrer.rc3.world:57923"
    "node04.dereferrer.rc3.world:57923"
    "node05.dereferrer.rc3.world:57923"
  ];
  prometheusTargets = mkTargets prometheusNodes;
  alertmanagerTargets = mkTargets alertmanagerNodes;
  grafanaTargets = mkTargets grafanaNodes;
  allTargets = mkTargets allNodes;
  smokepingTargets = mkTargets smokepingNodes;
  fulldiskencryptedTargets = mkTargets (forEach allNodes (node: node + ":2222"));

  makeFileSdJob = exporterName: {
    job_name = exporterName;
    scheme = "https";
    relabel_configs = removePort;
    basic_auth = {
      username = "prometheus";
      password = "\${${toUpper (builtins.replaceStrings ["-"] ["_"] exporterName)}}";
    };
    file_sd_configs = [{
      files = [ "/var/lib/prometheus-scrape-configs/${exporterName}.json" ];
    }];
  };
  exporters = config.rc3.monitoring.exporters ++ forEach config.rc3.monitoring.exporters (exporter: exporter+"-dev");

in [
  {
    job_name = "workadventure-backend";
    scheme = "https";
    relabel_configs = removePort;
    file_sd_configs = [{
      files = [ "/var/lib/prometheus-scrape-configs/workadventure-backend.json" ];
    }];
  }
  {
    job_name = "bigbluebutton";
    scheme = "https";
    relabel_configs = removePort;
    static_configs = [{ targets = import ./bbb_targets.nix; labels = { role="bigbluebutton"; cluster="infra.run"; }; }];
    metrics_path = "/bbb-exporter";
  }
  {
    job_name = "bigbluebutton-node";
    scheme = "http";
    relabel_configs = removePort;
    static_configs = [{ targets = forEach (import ./bbb_targets.nix) (target: target + ":9100"); labels = { role="bigbluebutton"; cluster="infra.run"; }; }];
  }
#  {
#    job_name = "bigbluebutton-selenium";
#    scrape_interval = "120s";
#    scheme = "http";
#    relabel_configs = [
#      {
#        source_labels = [ "__address__" ];
#        target_label = "__param_target";
#      }
#      {
#        source_labels = [ "__param_target" ];
#        target_label = "instance";
#      }
#      {
#        replacement = "bbb-selenium.infra.run:9123";
#        target_label = "__address__";
#      }
#    ];
#    static_configs = [{ targets = import ./bbb_targets.nix; labels = { role="bigbluebutton"; cluster="infra.run"; }; }];
#  }
  {
    job_name = "workadventure";
    scheme = "https";
    relabel_configs = removePort;
    file_sd_configs = [{
      files = [ "/var/lib/prometheus-scrape-configs/workadventure.json" ];
    }];
  }
  {
    job_name = "prometheus";
    scheme = "https";
    relabel_configs = removePort;
    basic_auth = {
      username = "monitoring";
      password = "\${PROMETHEUS}";
    };
    static_configs = prometheusTargets;
  }
  {
    job_name = "c3voc";
    scheme = "https";
    static_configs = [{ targets = [ "monitoring.c3voc.de" ]; }];
  }
  {
    job_name = "hubsite-node";
    scheme = "https";
    metrics_path = "/node_exporter";
    relabel_configs = removePort;
    basic_auth = {
      username = "monitoring";
      password = "\${POSTGRES}";
    };
    static_configs = [{ targets = import ./hubsite-targets.nix; }];
  }
  {
    job_name = "postgres";
    scheme = "https";
    basic_auth = {
      username = "monitoring";
      password = "\${POSTGRES}";
    };
    relabel_configs = removePort;
    static_configs = [{ targets = [
      "rc3-hub-db1.hub.rc3.world"
      "rc3-hub-db2.hub.rc3.world"
    ]; }];
  }
  {
    job_name = "rc3-tickets";
    scheme = "https";
    metrics_path = "/database-exporter/metrics";
    basic_auth = {
      username = "monitoring";
      password = "\${POSTGRES}";
    };
    relabel_configs = removePort;
    static_configs = [{ targets = [
      "rc3-hub-db1.hub.rc3.world"
      "rc3-hub-db2.hub.rc3.world"
    ]; }];
  }
  {
    job_name = "postgres-node";
    scheme = "https";
    metrics_path = "/node-exporter/metrics";
    basic_auth = {
      username = "monitoring";
      password = "\${POSTGRES}";
    };
    relabel_configs = removePort;
    static_configs = [{ targets = [
      "rc3-hub-db1.hub.rc3.world"
      "rc3-hub-db2.hub.rc3.world"
    ]; }];
  }
  {
    job_name = "alertmanager";
    scheme = "https";
    relabel_configs = removePort;
    basic_auth = {
      username = "monitoring";
      password = "\${ALERTMANAGER}";
    };
    static_configs = alertmanagerTargets;
  }
  {
    job_name = "grafana";
    scheme = "https";
    relabel_configs = removePort;
    static_configs = grafanaTargets;
  }
  {
    job_name = "nginx";
    scheme = "https";
    relabel_configs = removePort;
    static_configs = allTargets;
    metrics_path = "/nginx-exporter/metrics";
  }
  {
    job_name = "netbox";
    scheme = "https";
    static_configs = [{ targets = [ "netbox.infra.rc3.world" ]; }];
  }
  {
    job_name = "netbox-app";
    scheme = "https";
    metrics_path = "/api/plugins/metrics-ext/app-metrics";
    static_configs = [{ targets = [ "netbox.infra.rc3.world" ]; }];
  }
  {
    job_name = "node";
    scheme = "https";
    relabel_configs = removePort;
    static_configs = allTargets;
    metrics_path = "/node-exporter/metrics";
  }
  {
    job_name = "powerdns";
    scheme = "https";
    relabel_configs = removePort;
    static_configs = dnsTargets;
    metrics_path = "/powerdns-exporter/metrics";
  }
  {
    job_name = "dns";
    scheme = "https";
    inherit relabel_configs;
    static_configs = dnsTargets;
    metrics_path = "/blackbox-exporter/probe";
    params = { module = [ "https" ]; };
  }
  {
    job_name = "smokeping";
    scheme = "https";
    relabel_configs = removePort;
    static_configs = smokepingTargets;
    metrics_path = "smokeping-exporter/metrics";
  }
  {
    job_name = "icmp";
    scheme = "https";
    inherit relabel_configs;
    static_configs = allTargets;
    metrics_path = "/blackbox-exporter/probe";
    params = { module = [ "icmp" ]; };
  }
  {
    job_name = "https";
    scheme = "https";
    inherit relabel_configs;
    static_configs = allTargets;
    metrics_path = "/blackbox-exporter/probe";
    params = { module = [ "https" ]; };
  }
  {
    job_name = "disk-encryption";
    scheme = "https";
    inherit relabel_configs;
    static_configs = fulldiskencryptedTargets;
    metrics_path = "/blackbox-exporter/probe";
    params = { module = [ "ssh-banner"]; };
  }
  {
    job_name = "jitsi_loadbalancer";
    inherit relabel_configs;
    scheme = "https";
    static_configs = [{ targets = ["jitsi.rc3.world"]; labels = { "role" = "jitsi-loadbalancer";}; }];
    metrics_path = "/blackbox-exporter/probe";
    params = { module = [ "https" ]; };
  }
  {
    job_name = "blackbox_dynamic_https";
    inherit relabel_configs;
    scheme = "https";
    file_sd_configs = [{
      files = [ "/var/lib/prometheus-scrape-configs/blackbox_targets_https.json" ];
    }];
    metrics_path = "/blackbox-exporter/probe";
    params = { module = [ "https" ]; };
  }
  {
    job_name = "workadventure-blackbox";
    inherit relabel_configs;
    scheme = "https";
    file_sd_configs = [{
      files = [ "/var/lib/prometheus-scrape-configs/workadventure.json" ];
    }];
    metrics_path = "/blackbox-exporter/probe";
    params = { module = [ "https" ]; };
  }
  {
    job_name = "blackbox_dynamic_icmp";
    inherit relabel_configs;
    scheme = "https";
    file_sd_configs = [{
      files = [ "/var/lib/prometheus-scrape-configs/blackbox_targets_icmp.json" ];
    }];
    metrics_path = "/blackbox-exporter/probe";
    params = { module = [ "icmp" ]; };
  }
  {
    job_name = "dereferrer";
    relabel_configs = removePort;
    scheme = "http";
    static_configs = dereferrerTargets;
  }
  {
    job_name = "deployer";
    scheme = "http";
    static_configs = [{ targets = [ "deployer.rc3.world" ]; }];
  }
  {
    job_name = "bigbluebutton-blackbox";
    inherit relabel_configs;
    scheme = "https";
    static_configs = [{ targets = import ./bbb_targets.nix; }];
    metrics_path = "/blackbox-exporter/probe";
    params = { module = [ "https" ]; };
  }
] ++ forEach exporters makeFileSdJob

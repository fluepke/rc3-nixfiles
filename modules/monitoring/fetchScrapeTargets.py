#!/usr/bin/env python3
import json
import urllib.request
import argparse
import os
import sys

parser = argparse.ArgumentParser(description='Retrieve file_sd scrape configs from netbox')
parser.add_argument('--exporters', nargs='+', default=['node_exporter', 'wireguard_exporter'])
parser.add_argument('--netbox-url', nargs=1, default='https://netbox.infra.rc3.world')
parser.add_argument('--netbox-token')
parser.add_argument('--output-dir', default='/var/run/prometheus-scrape-configs')

args = parser.parse_args()

def isPrivate(ip):
    return ip.startswith('10.') or ip.startswith('172.16.') or ip.startswith('192.168.') or ip.startswith('fd')

if args.netbox_token == None:
    args.netbox_token = os.environ.get('NETBOX_TOKEN', None)

if args.netbox_token == None:
    print('No netbox token was supplied')
    sys.exit(1)

exporters = {}
request = urllib.request.Request(f"{ args.netbox_url }/api/ipam/services/?limit=10000", headers={'Authorization': f"Token {args.netbox_token}",'Content-Type': 'application/json','Accept': 'application/json'})
response = urllib.request.urlopen(request)

json_response = json.load(response)

for exporter in json_response['results']:
    suffix = exporter['description'].split('/')[-1]
    if suffix in args.exporters:
        labels = {}
        for tag in exporter['tags']:
            if str(tag['name']).startswith('label:'):
                labeltag=str(tag['name']).split(':')
                label=str(labeltag[1]).split('=')
                labels[str(label[0])] = str(label[1])
        labels['__metrics_path__'] = str(exporter['description'])
        if 'environment' in labels and labels['environment'] == 'dev':
            suffix += '-dev'
        if not suffix in exporters:
            exporters[suffix] = []
        if exporter['virtual_machine'] != None:
            exporters[suffix].append({'targets': [ str(exporter['virtual_machine']['name'])+':'+str(exporter['port'])],'labels': labels})
        if exporter['device'] != None:
            exporters[suffix].append({'targets': [ str(exporter['device']['name'])+':'+str(exporter['port'])],'labels': labels})

for name, configs in exporters.items():
    with open(f"{args.output_dir}/{name}.json", 'w') as datei:
        json.dump(configs, datei, indent=4)

request = urllib.request.Request(f"{ args.netbox_url }/api/virtualization/virtual-machines/?limit=10000",  headers={'Authorization': f"Token {args.netbox_token}",'Content-Type': 'application/json','Accept': 'application/json'})
response = urllib.request.urlopen(request)

json_response = json.load(response)

httpsTargets = []
icmpTargets = []

for vm in json_response['results']:
    if any(tag['name'] == 'blackbox_https' for tag in vm['tags']):
        httpsTargets.append({'targets': [ vm['name'] ], 'labels': {
            'vm': vm['name'],
            'role': vm['role']['slug'],
            'site': vm['site']['slug'] if (vm['site'] != None) else 'outer-space',
            'cluster': vm['cluster']['name'],
            'status': vm['status']['value'],
            'netbox_url': vm['url'],
            }})
    if any(tag['name'] == 'blackbox_icmp' for tag in vm['tags']):
        if 'address' in vm['primary_ip']:
            icmpTargets.append({'targets': [ vm['primary_ip']['address'].split('/')[0] ], 'labels': {
                'vm': vm['name'],
                'role': vm['role']['slug'],
                'site': vm['site']['slug'] if (vm['site'] != None) else 'outer-space',
                'cluster': vm['cluster']['name'],
                'status': vm['status']['value'],
                'address_family': 'ipv6',
                'netbox_url': vm['url'],
                }})
        if 'address' in vm['primary_ip4']:
            icmpTargets.append({'targets': [ vm['primary_ip4']['address'].split('/')[0] ], 'labels': {
                'vm': vm['name'],
                'role': vm['role']['slug'],
                'site': vm['site']['slug'] if (vm['site'] != None) else 'outer-space',
                'cluster': vm['cluster']['name'],
                'status': vm['status']['value'],
                'address_family': 'ipv4',
                'netbox_url': vm['url'],
                }})

with open(f"{args.output_dir}/blackbox_targets_https.json", 'w') as datei:
    json.dump(httpsTargets, datei, indent=4)

request = urllib.request.Request(f"{ args.netbox_url }/api/dcim/devices/?tag=blackbox_icmp&limit=1000", headers={'Authorization': f"Token {args.netbox_token}",'Content-Type': 'application/json','Accept': 'application/json'})
response = urllib.request.urlopen(request)

json_response = json.load(response)

for device in json_response['results']:
    request = urllib.request.Request(f"{ args.netbox_url }/api/ipam/ip-addresses/?device_id={ device['id'] }", headers={'Authorization': f"Token {args.netbox_token}",'Content-Type': 'application/json','Accept': 'application/json'})
    response = urllib.request.urlopen(request)
    
    json_response = json.load(response)

    for result in json_response['results']:
        if isPrivate(result['address'].split('/')[0]):
            continue
        icmpTargets.append({'targets': [ result['address'].split('/')[0] ], 'labels': {
            'role': device['device_role']['slug'] if (device['device_role'] != None) else 'none',
            'site': device['site']['slug'] if (device['site'] != None) else 'outer-space',
            'device': device['name'],
            'interface': result['assigned_object']['name'],
            'address_family': result['family']['label'].lower(),
            'netbox_url': device['url'],
        }})

with open(f"{args.output_dir}/blackbox_targets_icmp.json", 'w') as datei:
    json.dump(icmpTargets, datei, indent=4)
